# Sterling *VMware Tanzu Kubernetes Workshop* Lab Guide

<img src="images/Sterling-Logo-black.png" width="400"/><br>
<img src="images/VMW_09Q3_LOGO_Corp_Gray-01.png" width="400"/>


### Common Kubectl CLI Syntax

> Useful guide to basic kubectl CLI commands for interacting with any Kubernetes Cluster, and a few useful Tanzu links for more information
>> <a href="https://kubernetes.io/docs/reference/kubectl/cheatsheet/">Kubectl Cheat Sheet</a><br>
>> <a href="https://core.vmware.com/vmware-vsphere-tanzu"> Core VMware Tanzu Technical Articles </a><br>
>> <a href="https://tanzu.vmware.com/developer/"> Developer Tanzu Portal, Labs and Guides </a><br>
>> <a href="https://tanzu.vmware.com/tanzu"> Tanzu Overview Splash Page </a><br>

> Use to view which contexts you have access to
>> kubectl config get-contexts

> Use to switch into the context of your choice to interact with the intended cluster and namespace
>> kubectl config use-context context-name

> Use 'get' to view resources as they're realized on the cluster by object type
>> kubectl get 'cluster object'

> Use 'describe' to get in depth information on a particular object
>> kubectl describe 'cluster object' 'object name' 



### Variable Syntax

> A few of the commands will be unique to your lab and will have variables in place. Variables will be within '< >' to indicate a place where you will need to replace text.
        For example in this statement, your name is <NAME>, and would require you to replace <NAME> to be accurate.

> In the labs, the supervisor context you will be interacting with will match your lab username, therefore accessing the appropriate context will simply be replacing <username> with your assigned account. For example

        kubectl config use-context <username>

> An attendee assigned workshop01 would use

        kubectl config use-context workshop01



## Lab Setup


1. Access your Horizon environment using the instructions and account information provided by during the workshop

2. Open Powershell to clone this repository for access to required resources

3. Clone the repository to your virtual desktop

        git clone https://gitlab.com/bjamesdowning/sterling.git

4. Drop into the working directory

        cd sterling

5. Download the needed Tanzu CLI plugin for Kubectl and unzip the archive

        curl.exe -k https://tanzu.sterling.lab/wcp/plugin/windows-amd64/vsphere-plugin.zip -o vsphere-plugin.zip
        Expand-Archive .\vsphere-plugin.zip

6. Verify the version

        .\vsphere-plugin\bin\kubectl-vsphere.exe version

<img src="images/kubectl-vspere-version.png"/>

7. Alternatively, you can download the kubectl-vsphere plugin via a web interface

> Open Chrome and navigate to https://tanzu.sterling.lab
> Click 'Download CLI Plugin Windows'
> Extract the contents, place the binary in the executable path, and use as normal

<img src="images/kubectlvsphereplugin.png" width="400"/>

## Logging into the vSphere with Kubernetes Environment

1. Using the kubectl-vsphere plugin we just downloaded, log directly into the vSphere with Tanzu cluster where it will display what namespaces you have access to

        .\vsphere-plugin\bin\kubectl-vsphere.exe login -u <username>@sterling.lab --server=https://tanzu.sterling.lab --insecure-skip-tls-verify

<img src="images/powershell-login-vsphere-cluster.png" width="400"/>

2. Drop into the context of the same name as your use account

        kubectl config use-context <username>

> When logged in you should be shown a list of contexts that correlate to namespace for which you have access to

3. Verify access into the namespace

        kubectl get all

> this should display there are no resources in *username_namespace*


## Deploying Workloads to vSphere with Tanzu through the Kubernetes API


1. Deploy your first workload to vSphere with Tanzu, a simple NGINX webserver using imperative commands

        kubectl create deployment nginx-<username> --image=nginx

> In this instance we are creating a Deployment to manage a replicaset made up of ngxin pods. 

2. Now deploy a load balancer using K8s and realized through NSX as the front end to NGINX
    
        kubectl expose deployment nginx-<username> --port=80 --target-port=80 --type=LoadBalancer

> This command creates a kubernetes service of type LoadBalancer which is translated to NSX-T to create a Virtual IP and add all relevent pods as backend servers to balance traffic across

3. Verify the deployment was successfull by viewing the resources deployed and visiting the extneral IP via Chrome
    
        kubectl get deploy,po,svc

<img src="images/nginx-running.png"/>

## Viewing Resources from the vSphere Admin Perspective

1. Explore the reosurces created through vSphere

> vCenter

    https://sterling-hq-fx2-vc-01.sterling.lab

> NSX-T

    https://sterling-hq-fx2-nsxt.sterling.lab

2. Open Chrome, and navigate to https://sterling-hq-fx2-vc-01.sterling.lab and login with workshop credentials

        open up the datacenter > cluster > namspaces
        Explore your namespace and workload deployed from a vsphere perspective
    
<img src="images/vsphere-ui01.png"/>

3. Open Chrome, and navigate to https://sterling-hq-fx2-nsxt.sterling.lab and login with workshop credentials
    
        In the search bar at the top, type in the name of your K8s deployment nginx-<username>
        View all the object creates from your imperative commands

<img src="images/nsx-ui01.png"/>

> Load Balancer, segments, pods, services, etc

## Cleanup the Imperative Workload

1. Back in PowerShell, now delete the imperative nginx application
    
        kubectl delete deploy nginx-<username>
        kubectl delete svc nginx-<username>

> Verify object deletion by using vSphere/NSX UI, or the K8s API and Kubectl

## Deploy the same application declaratively

1. Navigate to the /sterling root folder

2. View the manifest *nginx-declarative.yaml*

> Inspect the manifest which contains two objects, a Deployment for the workload and Service to expose the web server

3. Apply the manifest
            
        kubectl apply -f nginx-declarative.yaml
        
4. Review the objecs created
            
        kubectl get deploy,po,svc

## Building Tanzu Kubernetes Clusters

> Tanzu Kubernetes Clusters are custom resource types within vSphere for Tanzu using the ClusterAPI project. Each cluster can act independently, however can be managed by the over arching supervisor cluster just as any other K8s object. 

1. Navigate to the root sterling foler
2. View the tkg-cluster-v1alpha2.yaml manifest
3. Apply the manifest to begin building of a dedicate Tanzu Kubernetes Cluster within your namespace
        
        kubectl apply -f tkg-cluster-v1alpha2.yaml

> This process takes up to 5-7 minutes to complete

4. Review creation of objects   

        kubectl get tkc
        kubectl get tkr
        kubectl get vm
        kubectl describe tkc tkgs-cluster01
        kubectl get svc

> TKC is the shortend name for the resource kind of TanzuKubernetesCluster which is the single object managing the Guest Kubernetes Clusters

> TKR is the shortend name for the resource kind of TanzuKubernetesReleases, the different versions available to your cluster as pulled from the content library used to back the namesapce
    
5. While the cluster is building, open chrome and navigate to https://sterling-hq-fx2-vc-01.sterling.lab and log in using your workshop credentials

6. View the actions within your namespace to confirm the deployment of virtual machines to build your Tanzu Kubernetes Cluster

## Explore different viewpoints from the vSphere admin perspective vs the Kubernetes admin perspetive

1. Open Chrome, and navigate to https://sterling-hq-fx2-nsxt.sterling.lab
        
        Go to Networking > Network Topology

<img src="images/nsx-ui02-topology.png"/>

2. Log into the newly deployed Tanzu Kubernetes Cluster
    
        .\vsphere-plugin\bin\kubectl-vsphere.exe login --server=https://tanzu.sterling.lab --tanzu-kubernetes-cluster-name tkgs-cluster --tanzu-kubernetes-cluster-namespace <username> -u <username>@sterling.lab --insecure-skip-tls-verify

<img src="images/powershell-login-tkc.png"/>
    
> A new context is now added to your kubeconfig allowing you to now shift directly into your TKG cluster

3. verify you're in the new cluster
        
        kubectl config use-context tkgs-cluster
        kubectl get nodes
    
4. Setting up some basic security parameters, navigate to the sterling root folder, review and deploy the appropriate clusterrole and rolebinding objects. 

> This is allowing the default service account to deploy resourcs on the TKG cluster. TKG clusters are deployed using Pod Security Policies as a security feature out of the box. 

    kubectl apply -f rolebindingforpsp.yaml
    kubectl apply -f clusterrole.yaml
       

## Deploying a microservice based application to the Tanzu Kubernetes Cluster

> Deploying a demonstration application initiating creation of compute, storage, and network resources all integrated directly with the application and abstracted through Kubernetes. 

1. Navigate to the sterling root folder and view the content of *yelbdemo.yaml*. There are several objects in this manifest, including a service of type loadbalancer, a persistent volume claim, deployments, and so on. All the components to make our application run successfully.

2. Verify you're in the correct context

        kubectl config use-context tkgs-cluster

3. Verify storage class availability as passed to your cluster through vSphere

        kubectl get sc

4. Deploy the entire application

        kubectl apply -f yelbdemo.yaml

5. Verify success

        kubectl get deploy,po,pvc,svc

<img src="images/yelb-verify.png"/>

> Deployment/Pod objects are the compute resources created, Service the network, and Persistent Volume Clain the storage. All three foundational requirements of our application deployed through a single manifest and single k8's api realized across vSphere + NSX

<img src="images/yelbpvc.png"/>

6. Open chrome and navigate to the external IP address provided by the LoadBalancer service, (10.49.0.7 in this case)

> Do some voting on the app to be sure it works.


## Lifecycle Management using vSphere with Tanzu

First lets revisit our original nginx-declarative deployment. Lets assume we're noticing latency caused by processing overload due to there only being a single pod serving all requests. Our goal is to scale the application horizontally to better distribute traffic.

1. Navigate to the folder where the manifest is stored, and open it up in windows explorer

        cd sterling
        ii .

2. Right click nginx-declarative.yaml and open with Notepadd++

3. Find and edit the 'replicas' field from 1 to 3. Save and reapply the manifest

> Be sure you're in the correct context since this app is deployed as vSphere pods, and not in our Tanzu Kubernetes Cluster

    kubectl config use-context <username>
    
> If you don't remember the context names you have access to, run
        
    kubectl config get-contexts
    
4. Apply the edited manifest to scale out our application

        kubectl apply -f nginx-declarative.yaml

5. Verify new pods are being created (both in the vCenter UI and through kubectl)

        kubectl get po

6. Verify the new pods are dynamically placed behind our already created LoadBalancer

        kubectl get endpoints

<img src="images/nginx-pod-scaleout.png"/>

> Now we can see there are three pods running in our deployment, and behind the loadbalancer created previously so load is being spread across different pods


Another common lifecycle task is upgrading kubernetes itself. In this scenario, you will upgrade your existing TKGs Cluster in a declarative manner, just as easily as scaling out the nginx application.
    
1. Navigate to the folder where the manifest is stored, and open it up in windows explorer

        cd sterling
        ii .

2. Right click tkg-cluster-v1aplha2.yaml and open with Notepadd++

3. Find and edit both 'tkr, reference, name' fields from v1.19 to
    
        v1.20.12---vmware.1-tkg.1.b9a42f3

<img src="images/upgrade-tkg-cluster.png" height="350" width="400"/>

4. Save and reapply the manifest

        kubectl apply -f tkg-cluster-v1alpha2.yaml

> To know the available version, see what Tanzu Kubernetes Releases are available, run
    
    kubectl get tkr

> And to understand the potential upgrades from existing clusters, run

    kubectl get tkc

With the manifest edited with the new version for both the control plane and node pools, re apply and watch the upgrade occur. In this case we only have a single control and single data node, however in a production environment this upgrade will roll through nodes adding, upgrading, and decommisioning older version to avoid downtown. 


> This process will take some time as you can watch new nodes being deployed to complete the upgrade. In the meantime, go back through the Kubernetes API (using kubectl), the vCenter and NSX UIs, to review all the infrastructure you've created simply through a few manifest files describing your intended application design.


This is the end of the lab, when finished please cleanup your environment simply be deleting the manifests deployed

    kubectl delete -f tkg-cluster-v1alpha2.yaml
    kubectl delete -f nginx-declarative.yaml









